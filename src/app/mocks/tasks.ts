import { Task } from '../classes/task';

export function getTasksMock(): Task[] {
  return [new Task('Buy milk', false, false), new Task('sleep', true, true), new Task('eat', false, false)];
}
