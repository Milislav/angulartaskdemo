export class Task {
  readonly title: string;
  readonly important: boolean;
  done: boolean;

  constructor(title: string, important: boolean = false, done: boolean = false) {
    this.title = title;
    this.important = important;
    this.done = done;
  }
}
