import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HelloWorldComponent } from './components/hello-world/hello-world.component';
import { PersonPreviewComponent } from './components/person-preview/person-preview.component';
import { TaskComponentComponent } from './components/task-component/task-component.component';

import { TaskSummaryComponent } from './components/task-summary/task-summary.component';
import { TaskCreateComponent } from './components/task-create/task-create.component';
import { TaskAppComponentComponent } from './components/task-app-component/task-app-component.component';

@NgModule({
  declarations: [
    AppComponent,
    HelloWorldComponent,
    PersonPreviewComponent,
    TaskComponentComponent,
    TaskSummaryComponent,
    TaskCreateComponent,
    TaskAppComponentComponent
  ],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
