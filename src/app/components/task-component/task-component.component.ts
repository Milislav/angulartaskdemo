import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-task-component',
  templateUrl: './task-component.component.html',
  styleUrls: ['./task-component.component.css']
})
export class TaskComponentComponent {
  @Input()
  title: string | undefined;

  @Input()
  important: boolean | undefined;

  @Input()
  done: boolean | undefined;

  @Output()
  toggleDone: EventEmitter<boolean> = new EventEmitter();

  constructor() {}

  onClick(): void {
    this.toggleDone.emit(!this.done);
  }
}
