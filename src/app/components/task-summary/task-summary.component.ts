import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-task-summary',
  templateUrl: './task-summary.component.html',
  styleUrls: ['./task-summary.component.css']
})
export class TaskSummaryComponent {
  constructor() {}

  @Input()
  numberOfUnfinishedTasks: number | undefined;
}
