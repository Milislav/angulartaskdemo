import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-person-preview',
  templateUrl: './person-preview.component.html',
  styleUrls: ['./person-preview.component.css']
})
export class PersonPreviewComponent implements OnInit {
  @Input()
  firstName: string | undefined;
  @Input()
  lastName: string | undefined;
  @Input()
  age: number | undefined;

  @Output()
  personClicked: EventEmitter<string> = new EventEmitter();

  personClick(): void {
    this.personClicked.emit(`${this.firstName} has been clicked`);
  }
  constructor() {}

  ngOnInit(): void {}
}
