import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-task-create',
  templateUrl: './task-create.component.html',
  styleUrls: ['./task-create.component.css']
})
export class TaskCreateComponent {
  constructor() {}

  @Output()
  createTask: EventEmitter<[string, boolean]> = new EventEmitter();

  onSubmit(event: any, title: string, important: boolean): void {
    event.preventDefault();
    if (title.length != 0) this.createTask.emit([title, important]);
  }
}
