import { Component, OnInit } from '@angular/core';
import { getTasksMock } from '../../mocks/tasks';
import { Task } from '../../classes/task';
import { TaskServiceService } from '../../services/task-service.service';

@Component({
  selector: 'app-task-app-component',
  templateUrl: './task-app-component.component.html',
  styleUrls: ['./task-app-component.component.css']
})
export class TaskAppComponentComponent implements OnInit {
  constructor(public taskService: TaskServiceService) {}

  ngOnInit(): void {}
}
