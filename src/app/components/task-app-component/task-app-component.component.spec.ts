import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskAppComponentComponent } from './task-app-component.component';

describe('TaskAppComponentComponent', () => {
  let component: TaskAppComponentComponent;
  let fixture: ComponentFixture<TaskAppComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TaskAppComponentComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskAppComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
