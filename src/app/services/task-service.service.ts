import { Injectable } from '@angular/core';
import { Task } from '../classes/task';
import { getTasksMock } from '../mocks/tasks';

@Injectable({
  providedIn: 'root'
})
export class TaskServiceService {
  constructor() {}
  tasks: Task[] = getTasksMock();

  addTask(taskOptions: [string, boolean]): void {
    const task = new Task(taskOptions[0], taskOptions[1]);
    this.tasks.push(task);
  }

  getNumberOfUnfinishedTasks(): number {
    return this.tasks.filter(task => !task.done).length;
  }
}
